#include <jni.h>
#include <string>
#include "header/MemoryFile.h"

MemoryFile *memoryFile ;
/*

extern "C"
JNIEXPORT void JNICALL
Java_com_macoli_log_1kid_LogKid_init(JNIEnv *env, jobject thiz, jstring log_path) {
    if (memoryFile != nullptr) {
        return ;
    }
    const char *buffer_path = env->GetStringUTFChars(log_path, 0);
    memoryFile = new MemoryFile(*buffer_path) ;
}
extern "C"
JNIEXPORT void JNICALL
Java_com_macoli_log_1kid_LogKid_writeLog(JNIEnv *env, jobject thiz, jstring tag,
                                         jstring message) {

}*/

extern "C"
JNIEXPORT void JNICALL
Java_com_macoli_log_1kid_LogKid_initLogKid(JNIEnv *env, jclass clazz, jobject log_kid_config) {
    jclass configCls = env->FindClass("com/macoli/log_kid/LogKidConfig") ;
    jfieldID logPathFiledID = env->GetFieldID(configCls, "logPath", "Ljava/lang/String;");
    auto logPath = (jstring)env->GetObjectField(log_kid_config, logPathFiledID);
    const char *buffer_log_path = env->GetStringUTFChars(logPath, 0);
    jfieldID  maxSizeFiledID = env->GetFieldID(configCls , "maxSize" , "I") ;
    auto  maxSize = env->GetIntField(log_kid_config , maxSizeFiledID) ;

    memoryFile = new MemoryFile(*buffer_log_path , maxSize) ;
}
extern "C"
JNIEXPORT void JNICALL
Java_com_macoli_log_1kid_LogKid_writeLog(JNIEnv *env, jclass clazz, jstring tag, jstring message) {
    const char *buffer_tag = env->GetStringUTFChars(tag, 0);
    const char *buffer_message = env->GetStringUTFChars(message, 0);
    memoryFile->writeLog(buffer_tag , buffer_message) ;
}