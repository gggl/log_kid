//
// Created by DS331B3 on 2023/3/11.
//

#ifndef LOG_KID_ANDROIDLOG_H
#define LOG_KID_ANDROIDLOG_H

#include <android/log.h>

#define LOG_TAG "log_kid"

#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)

#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#endif //LOG_KID_ANDROIDLOG_H
