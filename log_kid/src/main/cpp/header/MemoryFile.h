//
// Created by DS331B3 on 2023/3/11.
//

#ifndef LOG_KID_MEMORYFILE_H
#define LOG_KID_MEMORYFILE_H



class MemoryFile {
public:
    MemoryFile(const char &path , int maxSize);
    bool writeLog(const char *tag , const char *msg) ;

private:
    bool grow() ;
    void reloadFromFile() ;
    bool truncate(int size);
    void writeInt(int &value) ;
    void writeStr(const char* value , int &size) ;
    bool smmap() ;
    bool zeroFillSize(int size) ;
    bool mkPath(std::string path) ;
private:
    int pageSize ;          //页大小
    std::string logFilePath ;   //文件路径
    uint8_t *ptr ;
    int actualSize ;    //文件内容大小
    int fileSize ;  //文件大小
    int fd ;
    int maxSize ;
};

#endif //LOG_KID_MEMORYFILE_H
