package com.macoli.log_kid;

public class LogKid {

    // Used to load the 'log_kid' library on application startup.
    static {
        System.loadLibrary("log_kid");
    }

    public static native void initLogKid(LogKidConfig logKidConfig) ;
    public static native void writeLog(String tag , String message) ;
}