package com.macoli.log_kid;

public class LogKidConfig {
    public LogKidConfig(String logPath , int maxSize) {
        this.logPath = logPath;
        this.maxSize = maxSize ;
    }

    public String logPath ;
    public int maxSize ;    //MB
}
