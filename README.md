# log_kid

#### 介绍
Android使用mmap提速本地日志打印



#### 使用说明

1.  LogKid.initLogKid(LogKidConfig logKidConfig) 进行初始化，传入log文件路径和文件最大限制。
2.  LogKid.writeLog(String tag , String message) 日志写入
