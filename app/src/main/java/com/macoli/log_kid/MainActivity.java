package com.macoli.log_kid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    LogKid nativeLib = new LogKid() ;
    TextView consumeTimeTv , bioConsumeTimeTv ;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd") ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        consumeTimeTv = findViewById(R.id.consume_time_tv) ;
        bioConsumeTimeTv = findViewById(R.id.bio_consume_time_tv) ;

        findViewById(R.id.init_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                nativeLib.init(new File(getExternalFilesDir(null).getPath(), "log.txt").getPath());
                LogKid.initLogKid(new LogKidConfig(getExternalFilesDir(null).getPath() + "/111.txt", 4));
            }
        });

        findViewById(R.id.write_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long startTime = System.currentTimeMillis() ;
                for (int i = 0 ; i < 10000 ; ++i) {
                    nativeLib.writeLog("macoli" , "ddddaljflasjfoewujknkkkk");
                }
                consumeTimeTv.setText("mmap: " + (System.currentTimeMillis() - startTime) + "ms");
            }
        });

        findViewById(R.id.bio_write_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long startTime = System.currentTimeMillis() ;
                try {

                    String tag = "macoli" ;
                    String msg = "ddddaljflasjfoewujknkkkkgggggggggggggggggs" ;
                    FileOutputStream fos = new FileOutputStream(new File(getExternalFilesDir(null).getPath(), "log.txt").getPath()) ;
                    for (int i = 0 ; i < 100000 ; ++i) {
                        fos.write(tag.getBytes());
                        fos.write(msg.getBytes());
                    }
                    fos.close() ;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bioConsumeTimeTv.setText("bio: " + (System.currentTimeMillis() - startTime) + "ms");
            }
        });
    }
}